// O (n^2 * m)  - me

#include <algorithm>
#include <iostream>
#include <cassert>
#include <vector>

using namespace std;

int main() {
    cin.tie(nullptr)->sync_with_stdio(false);

    int n, m;
    cin >> n >> m;

    vector<vector<int> > a(n + 1, vector<int> (m + 1));
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= m; ++j) {
            cin >> a[i][j];
        }
    }

    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= m; ++j) {
            a[i][j] += a[i - 1][j];
        }
    }

    int x1 = 1;
    int y1 = 1;
    int x2 = 1;
    int y2 = 1;
    int ans = a[1][1];

    for (int t = 1; t <= n; ++t) { // top
        for (int b = t; b <= n; ++b) { // bottom
            int cur_sum = a[b][0] - a[t - 1][0]; assert(cur_sum == 0);
            int min_x = 0;
            int min_sum = 0;

            for (int r = 1; r <= m; ++r) { // right
                cur_sum += a[b][r] - a[t - 1][r];

                if (cur_sum - min_sum > ans) {
                    ans = cur_sum - min_sum;
                    x1 = t;
                    x2 = b;
                    y1 = min_x + 1;
                    y2 = r;
                }

                if (cur_sum < min_sum) {
                    min_sum = cur_sum;
                    min_x = r;
                }
            }
        }   
    }

    cout << ans << ": (" << x1 << ", " << y1 << ") -> (" << x2 << ", " << y2 << ")" << endl;
    for (int i = x1; i <= x2; ++i) {
        for (int j = y1; j <= y2; ++j) {
            cout << a[i][j] - a[i - 1][j] << " ";
        }
        cout << endl;
    }
}