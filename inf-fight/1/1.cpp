// O(n) - N.

#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, a, sum = 0, ans;
    bool f = true;
    
    cin >> n;
    
    for (int i = 0; i < n; ++i) {
        cin >> a;

        if (sum + 1 < a && f) {
            f = false;
            ans = sum + 1;
        }
        sum += a;
    }

    if (f) {
        ans = sum + 1;
    }

    cout << ans;
    return 0;
}