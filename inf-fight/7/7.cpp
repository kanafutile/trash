// O(nm) - F. suka

#include <bits/stdc++.h>
#define MAXN 1007

using namespace std;

int n, m;
int ans;
int ans_x = -1, ans_y = -1;
int lay;
int dp[2][MAXN];

void info(vector<int>& v) {
    for (auto el : v) {
        cout << el << " ";
    } cout << endl;
}

void start() {
    cin.tie(nullptr);
    ios_base::sync_with_stdio(false);
}

int main() {
    start();

    cin >> n >> m;

    for (int i = 1; i <= n; ++i) {
        lay = 1 - lay;
        fill(dp[lay], dp[lay] + MAXN, 0);
        for (int j = 1; j <= m; ++j) {
            char c;
            cin >> c;
            
            int x = c - '0';

            if (x) {
                dp[lay][j] = 1 + min({dp[lay][j - 1], dp[1 - lay][j], dp[1 - lay][j - 1]});

                if (dp[lay][j] > ans) {
                    ans = dp[lay][j];
                    ans_x = i;
                    ans_y = j;
                }
            }

        }
    }

    if (ans_x != -1) {
        cout << "(" << ans_x - ans + 1 << ", " << ans_y - ans + 1 << ") -> (" << ans_x << ", " << ans_y << ")" << endl;
        cout << "size: " << ans << endl;
    } else {
        cout << "none" << endl;
    }
}