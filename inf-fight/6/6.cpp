// O(n) - N.

#include <bits/stdc++.h>
#define ll long long

using namespace std;

int main(){
    ll n, i, cnt;
    string a, b;

    cin >> n >> a;
    cnt = 1;

    for(i = 1;i < n;i ++){
        cin >> b;
        if (cnt == 0) {
            cnt = 1;
            a = b;
        } else if (a == b){
            ++cnt;
        } else {
            --cnt;
        }
    }

    cout << a << endl;
    return 0;
}