// O(n + m) - N.

#include <bits/stdc++.h>

using namespace std;

int main() {
    int n, m, k;

    cin >> n >> m >> k;

    vector<vector<int> > a(n + 1, vector<int> (m + 1));

    int i, j;
    for (i = 1; i <= n; ++i) {
        for (j = 1; j <= m; ++j) {
            cin >> a[i][j];
        }
    }

    j = m;
    for (i = 1; i <= n; ++i) {
        while (a[i][j] > k && j > 1) {
            --j;
        }

        if (a[i][j] == k) {
            cout << "found: " << i << " " << j;
            return 0;
        }
    }

    cout << "not found";
    return 0;
}