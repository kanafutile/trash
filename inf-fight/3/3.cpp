// O(N) - N.

#include <bits/stdc++.h>
#define ll long long

using namespace std;

int main(){
    ll n, m, i;
    cin >> n >> m;

    ll a[n + m];

    for (i = 0; i < n + m; ++i) {
       cin >> a[i];
    }

    reverse(a, a + n + m);
    reverse(a, a + m);
    reverse(a + m, a + n + m);
    
    for (i = 0; i < n + m; ++i) {
        cout << a[i] << " ";
    }

    return 0;
}