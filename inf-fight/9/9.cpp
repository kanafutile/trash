// O(MlogN) - A.

#include <iostream>
#include <vector>
#include <algorithm>
 
#define endl "\n"
#define int long long
#define double long double
 
using namespace std;
 
void hello() {
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
#ifndef ONLINE_JUDGE
    freopen("input.txt", "r", stdin);
    freopen("output.txt", "w", stdout);
#endif
    cout.setf(ios::fixed);
    cout.precision(5);
}

const int INF = 1e9;
vector<int> a;

struct node {
    int left, right, add, min;
    node *ch_l, *ch_r;
};

node *build(int l, int r) {
    node *res = new node;
    res->left = l;
    res->right = r;
    if (l == r) {
        res->add = a[l - 1];
        res->min = a[l - 1];
        res->ch_l = res->ch_r = nullptr;
        return res;
    }
    int mid = (l + r) / 2;
    res->add = 0;
    res->ch_l = build(l, mid);
    res->ch_r = build(mid + 1, r);
    res->min = min(res->ch_l->min, res->ch_r->min);
    return res;
}

void update(node *root, int i, int j) {
    if (root->right < i || root->left > j)
        return;
    if (i <= root->left && root->right <= j) {
        root->add -= 1;
        root->min -= 1;
        return;
    }

    update(root->ch_l, i, j);
    update(root->ch_r, i, j);
    root->min = min(root->ch_l->min, root->ch_r->min) + root->add;
}

signed main() {
    hello();
 
    int n, m;
    cin >> n >> m;
    a.resize(n);

    for (int i = 0; i < n; ++i)
        cin >> a[i];

    vector<pair<int, int> > b(m);
    vector<vector<int> > left(n), right(n);

    for (int i = 0; i < m; ++i) {
        cin >> b[i].first >> b[i].second;
        b[i].first--, b[i].second--;
        left[b[i].second].push_back(b[i].first);
        right[b[i].first].push_back(b[i].second);
    }

    int min_elem = a[0];
    vector<int> ans(n, -INF), add(n, 0);
    node *root_l = build(1, n), *root_r = build(1, n);

    for (int i = 0; i < n; ++i) {
        ans[i] = max(ans[i], a[i] - min_elem);
        for (auto ind: left[i])
            update(root_l, ind + 1, i + 1);
        min_elem = root_l->min;
    }

    add.assign(n, 0);  
    min_elem = a[n - 1];

    for (int i = n - 1; i >= 0; --i) {
        ans[i] = max(ans[i], a[i] - min_elem);
        for (auto ind: right[i])
            update(root_r, i + 1, ind + 1);
        min_elem = root_r->min;
    }

    int res = *max_element(ans.begin(), ans.end());
    vector<int> segments;

    for (int i = 0; i < n; ++i)
        if (ans[i] == res) {
            for (int j = 0; j < m; ++j)
                if (!(b[j].first <= i && i <= b[j].second))
                    segments.push_back(j);
            break;
        }

    cout << res << endl << segments.size() << endl;
    for (auto item: segments)
        cout << item + 1 << " ";
    cout << endl;
 
    return 0;
}
