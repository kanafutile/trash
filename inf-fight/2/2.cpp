// O(n log n) - me

#include <iostream>
#include <vector>

using namespace std;
using int_t = uint64_t;

int main() {
    cin.tie(nullptr)->sync_with_stdio(false);

    int_t n;
    cin >> n;

    vector<int_t> a;
    a.reserve(n);
    for (int_t i = 0; i < n; ++i) {
        int_t x;
        cin >> x;
        a.emplace_back(x);
    }


    int_t l = 1;
    int_t r = n + 2;

    auto check = [&a] (int_t m) -> bool {
        int_t k = 0;
        for (auto i: a) {
            if (i < m) {
                ++k;
            }
        }

        return k == m - 1;
    };

    while (r - l > 1) {
        int_t m = l + (r - l) / 2;

        if (check(m)) {
            l = m;
        } else {
            r = m;
        }
    }

    cout << l << endl;
}