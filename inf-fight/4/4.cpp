// O(n) - E.

#include <stdio.h>
#include <bits/stdc++.h>
using namespace std;

#define amen ;
#define vec vector
#define pb push_back
#define all(x) x.begin(), x.end()
#define rall(x) x.rbegin(), x.rend()
#define Str(x) to_string(x)
#define len(s) (int)s.size()

typedef long long ll;
typedef double d;
typedef long double lld;
typedef string str;
typedef unsigned long long ull;

int joseph (int n, int k) {
    int res = 0;
    for (int i=1; i<=n; ++i)
        res = (res + k) % i;
    return res + 1;
}

int main()
{
    ios_base::sync_with_stdio(false);
    cin.tie(NULL);
    int n, k;
    cin >> n >> k;
    cout << joseph(n, k);

    return 0;
}